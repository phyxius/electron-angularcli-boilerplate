import { Component } from '@angular/core';
import { ElectronService } from 'ngx-electron';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})

export class AppComponent {

  constructor(private _electronService: ElectronService) {}
  title = 'app';
  name: string;
  message: string;
  onClick() {
      this.message = 'Hello ' + this.name;
      this.beep();
  }

  public beep() {
      this._electronService.shell.beep();
      this._electronService.shell.showItemInFolder('C:\Users');
  }
}
